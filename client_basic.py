#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
IP = sys.argv[1]
PORT = int(sys.argv[2])
MESSAGE = sys.argv[3:]
MESSAGE1 = MESSAGE[0]
MESSAGE2 = MESSAGE[1]


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            print(MESSAGE1 + ' ' + MESSAGE2)
            my_socket.send(bytes(MESSAGE1 + MESSAGE2, 'utf-8'))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))

        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
